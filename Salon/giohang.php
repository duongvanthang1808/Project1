<?php require 'command/cart.php';?>
<!DOCTYPE html>
<html>

<head>
<title>Shopin A Ecommerce Category Flat Bootstrap Responsive Website
	Template | Checkout :: w3layouts</title>
<link href="client/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- Custom Theme files -->
<!--theme-style-->
<link href="client/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Shopin Responsive web template, Bootstrap Web Templates, Flat Web Templates, AndroId Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="client/css/style4.css" rel="stylesheet" type="text/css"
	media="all" />
<!--//theme-style-->
<script src="client/js/jquery.min.js"></script>
<!--- start-rate---->
<script src="client/js/jstarbox.js"></script>
<link rel="stylesheet" href="client/css/jstarbox.css" type="text/css"
	media="screen" charset="utf-8" />
<script type="text/javascript">
            jQuery(function () {
                jQuery('.starbox').each(function () {
                    var starbox = jQuery(this);
                    starbox.starbox({
                        average: starbox.attr('data-start-value'),
                        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                        ghosting: starbox.hasClass('ghosting'),
                        autoUpdateAverage: starbox.hasClass('autoupdate'),
                        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                        stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function (event, value) {
                        if (starbox.hasClass('random')) {
                            var val = Math.random();
                            starbox.next().text(' ' + val);
                            return val;
                        }
                    })
                });
            });
        </script>
<!---//End-rate---->
</head>
<body>
	<!--header-->
        <?php include 'command/headerclient.php'; ?>
        <!--banner-->
	<div class="banner-top">
		<div class="container">
			<h1>Checkout</h1>
			<em></em>
			<h2>
				<a href="index.html">Home</a><label>/</label>Checkout</a>
			</h2>
		</div>
	</div>
	<!--login-->
	<script>$(document).ready(function (c) {
                $('.close1').on('click', function (c) {
                    $('.cart-header').fadeOut('slow', function (c) {
                        $('.cart-header').remove();
                    });
                });
            });
        </script>
	<script>$(document).ready(function (c) {
                $('.close2').on('click', function (c) {
                    $('.cart-header1').fadeOut('slow', function (c) {
                        $('.cart-header1').remove();
                    });
                });
            });
        </script>
	<script>$(document).ready(function (c) {
                $('.close3').on('click', function (c) {
                    $('.cart-header2').fadeOut('slow', function (c) {
                        $('.cart-header2').remove();
                    });
                });
            });
        </script>
	<div class="container">
		<div class="check-out">
			<div class="bs-example4" data-example-id="simple-responsive-table">
				<div class="table-responsive">
				<?php
				
				if (isset ( $_SESSION ['CART'] ) && $_SESSION ['CART'] != null) {
					
					$cart = $_SESSION ['CART'];
					
					?>
					<table class="table-heading simpleCart_shelfItem">
						<tr>
							<th class="table-grid">STT</th>

							<th>Giá</th>
							<th>Vận chuyển</th>
							<th>Số lượng</th>
							<th>Tổng</th>
							<th></th>
						</tr>
						<?php foreach ($cart as $value){?>
						<tr class="cart-header">

							<td><?php echo $value->sanpham['TenSanPham'];?></td>
							<td><?php echo $value->sanpham['Gia'];?></td>
							<td>Miễn phí</td>
							<td><input type="text" name="quantity" id="quantity"
								value="<?php echo $value->quantity?>" /></td>


							<td><?php echo $value->quantity * $value->sanpham['Gia'] ." đ" ?></td>
							<td><input type="button"
								data-id="<?php echo $value->sanpham['MaSanPham']?>"
								class="hvr-skew-backward editcart" value="Sửa" /></td>

							<td><input type="button"
								data-id="<?php echo $value->sanpham['MaSanPham']?>"
								class="hvr-skew-backward deletecart" value="Xóa" /></td>
							<script src="client/js/cart.js"></script>
						</tr>
						
						<?php }?>
						
						
						
					</table>
					<?php }else{?>
					<p>Giỏ hàng trống</p>
					<?php }?>
				</div>
			</div>
			<div class="produced">
				<a href="single.html" class="hvr-skew-backward">Mua sản phẩm</a>
			</div>
		</div>
	</div>

	<!--//login-->
	<!--brand-->
	<div class="container">
		<div class="brand">
			<div class="col-md-3 brand-grid">
				<img src="client/images/ic.png" class="img-responsive" alt="">
			</div>
			<div class="col-md-3 brand-grid">
				<img src="client/images/ic1.png" class="img-responsive" alt="">
			</div>
			<div class="col-md-3 brand-grid">
				<img src="client/images/ic2.png" class="img-responsive" alt="">
			</div>
			<div class="col-md-3 brand-grid">
				<img src="client/images/ic3.png" class="img-responsive" alt="">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!--//brand-->
	</div>

	</div>
	<!--//content-->
	<!--//footer-->
<?php include 'command/footerclient.php'; ?>
<!--//footer-->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

	<script src="client/js/simpleCart.min.js"></script>
	<!-- slide -->
	<script src="client/js/bootstrap.min.js"></script>

</body>
</html>