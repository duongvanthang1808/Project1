$('.deletecart').click(function() {
	$.ajax({
		url : 'giohangdelete.php',
		type : 'post',
		dataType : 'json',
		data : {
			id : $(this).data('id')
		},
		success : function(result) {
			if (result)
				window.location = "giohang.php";

		}
	});
});

$('.editcart').click(function() {

	var quantity = $('#quantity').val();

	$.ajax({
		url : 'giohangedit.php',
		type : 'post',
		dataType : 'json',
		data : {
			id : $(this).data('id'),
			quantity : quantity
		},
		success : function(result) {
			if (result)
				window.location = "giohang.php";
		}
	});
});
