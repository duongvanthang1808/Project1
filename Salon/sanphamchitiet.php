
<!DOCTYPE html>
<html>
<head>
<title>Shopin A Ecommerce Category Flat Bootstrap Responsive Website
	Template | Single :: w3layouts</title>
<link href="client/css/bootstrap.css" rel="stylesheet" type="text/css"
	media="all" />
<!-- Custom Theme files -->
<!--theme-style-->
<link href="client/css/style.css" rel="stylesheet" type="text/css"
	media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="Shopin Responsive web template, Bootstrap Web Templates, Flat Web Templates, AndroId Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--theme-style-->
<link href="client/css/style4.css" rel="stylesheet" type="text/css"
	media="all" />
<!--//theme-style-->
<script src="client/js/jquery.min.js"></script>
<!--- start-rate---->
<script src="client/js/jstarbox.js"></script>
<link rel="stylesheet" href="client/css/jstarbox.css" type="text/css"
	media="screen" charset="utf-8" />
<script type="text/javascript">
            jQuery(function () {
                jQuery('.starbox').each(function () {
                    var starbox = jQuery(this);
                    starbox.starbox({
                        average: starbox.attr('data-start-value'),
                        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                        ghosting: starbox.hasClass('ghosting'),
                        autoUpdateAverage: starbox.hasClass('autoupdate'),
                        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                        stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function (event, value) {
                        if (starbox.hasClass('random')) {
                            var val = Math.random();
                            starbox.next().text(' ' + val);
                            return val;
                        }
                    })
                });
            });
        </script>
<!---//End-rate---->
<link href="client/css/form.css" rel="stylesheet" type="text/css"
	media="all" />
</head>
<body>
	<!--header-->
        <?php include 'command/headerclient.php'; ?>
        <!--banner-->
	<div class="banner-top">
		<div class="container">
			<h1>Single</h1>
			<em></em>
			<h2>
				<a href="index.html">Home</a><label>/</label>Single</a>
			</h2>
		</div>
	</div>
	<div class="single">

            <?php
												$con = new database ();
												$sql = "SELECT * FROM sanpham WHERE MaSanPham = " . "'" . $_GET ['masp'] . "'";
												
												$chitiet = $con->select_query ( $sql );
												?>
            <div class="container">
			<div class="col-md-9">
				<div class="col-md-5 grid">
					<div class="flexslider">
						<ul class="slides">
							<li data-thumb="client/images/<?php echo $chitiet['Hinh']; ?>">
								<div class="thumb-image">
									<img src="client/images/<?php echo $chitiet['Hinh']; ?>"
										data-imagezoom="true" class="img-responsive">
								</div>
							</li>

						</ul>
					</div>
				</div>
				<div class="col-md-7 single-top-in">
					<div class="span_2_of_a1 simpleCart_shelfItem">
						<h3><?php echo $chitiet['TenSanPham']; ?></h3>
						<p class="in-para">Sản phẩm tốt nhất của chúng tôi</p>
						<div class="price_single">
							<span class="reducedfrom item_price"><?php echo $chitiet['Gia']; ?></span>

							<div class="clearfix"></div>
						</div>
						<h4 class="quick">Giới thiệu:</h4>
						<p class="quick_desc"><?php echo $chitiet['TieuDe']; ?></p>
						<div class="wish-list">
							
							<li><a href="giohanged.php?masp=<?php echo $chitiet['MaSanPham']?>"
								class="hvr-skew-backward">Thêm giỏ hàng</a></li>

						</div>

						<!--quantity-->
						<script>
                                $('.value-plus').on('click', function () {
                                    var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) + 1;
                                    divUpd.text(newVal);
                                });

                                $('.value-minus').on('click', function () {
                                    var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.text(), 10) - 1;
                                    if (newVal >= 1)
                                        divUpd.text(newVal);
                                });
                            </script>
						<!--quantity-->


						<div class="clearfix"></div>
					</div>

				</div>
				<div class="clearfix"></div>
				<!---->

				<!---->
			</div>
			<!----->

			<div class="col-md-3 product-bottom product-at">
				<!--categories-->
				<div class=" rsidebar span_1_of_left">
					<h4 class="cate">Categories</h4>
					<ul class="menu-drop">

                            <?php
																												$sql = "SELECT * FROM `danhmuc` WHERE 1";
																												
																												$sanpham = $con->select_all_query ( $sql );
																												
																												if (! $sanpham) {
																													echo "Chưa có sản phẩm trong danh mục này";
																												} else {
																													foreach ( $sanpham as $type ) {
																														?>
                                    <li class="item1"><a
							href="sanpham.php?loai=<?php echo $type['MaDanhMuc']; ?>"><?php echo $type['TenDanhMuc']; ?></a>
						</li>
                                    <?php
																													}
																												}
																												?>
                        </ul>
				</div>
				<!--initiate accordion-->
				<script type="text/javascript">
                        $(function () {
                            var menu_ul = $('.menu-drop > li > ul'),
                                    menu_a = $('.menu-drop > li > a');
                            menu_ul.hide();
                            menu_a.click(function (e) {
                                e.preventDefault();
                                if (!$(this).hasClass('active')) {
                                    menu_a.removeClass('active');
                                    menu_ul.filter(':visible').slideUp('normal');
                                    $(this).addClass('active').next().stop(true, true).slideDown('normal');
                                } else {
                                    $(this).removeClass('active');
                                    $(this).next().stop(true, true).slideUp('normal');
                                }
                            });

                        });
                    </script>
				<!--//menu-->

			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!--brand-->
	<div class="container">
		<div class="brand">
			<div class="col-md-3 brand-grid">
				<img src="client/images/ic.png" class="img-responsive" alt="">
			</div>
			<div class="col-md-3 brand-grid">
				<img src="client/images/ic1.png" class="img-responsive" alt="">
			</div>
			<div class="col-md-3 brand-grid">
				<img src="client/images/ic2.png" class="img-responsive" alt="">
			</div>
			<div class="col-md-3 brand-grid">
				<img src="client/images/ic3.png" class="img-responsive" alt="">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!--//brand-->
	</div>

	</div>
	<!--//content-->
	<!--//footer-->
<?php include 'command/footerclient.php'; ?>
<!--//footer-->
	<script src="client/js/imagezoom.js"></script>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script defer src="client/js/jquery.flexslider.js"></script>
	<link rel="stylesheet" href="client/css/flexslider.css" type="text/css"
		media="screen" />

	<script>
// Can also be used with $(document).ready()
                            $(window).load(function () {
                                $('.flexslider').flexslider({
                                    animation: "slide",
                                    controlNav: "thumbnails"
                                });
                            });
</script>

	<script src="client/js/simpleCart.min.js"></script>
	<!-- slide -->
	<script src="client/js/bootstrap.min.js"></script>


</body>
</html>