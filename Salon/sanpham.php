
<!DOCTYPE html>
<html>
    <head>
        <title>Shopin A Ecommerce Category Flat Bootstrap Responsive Website Template | Products :: w3layouts</title>
        <link href="client/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Custom Theme files -->
        <!--theme-style-->
        <link href="client/css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Shopin Responsive web template, Bootstrap Web Templates, Flat Web Templates, AndroId Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--theme-style-->
        <link href="client/css/style4.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <script src="client/js/jquery.min.js"></script>
        <!--- start-rate---->
        <script src="client/js/jstarbox.js"></script>
        <link rel="stylesheet" href="client/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
        <script type="text/javascript">
            jQuery(function () {
                jQuery('.starbox').each(function () {
                    var starbox = jQuery(this);
                    starbox.starbox({
                        average: starbox.attr('data-start-value'),
                        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                        ghosting: starbox.hasClass('ghosting'),
                        autoUpdateAverage: starbox.hasClass('autoupdate'),
                        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                        stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function (event, value) {
                        if (starbox.hasClass('random')) {
                            var val = Math.random();
                            starbox.next().text(' ' + val);
                            return val;
                        }
                    })
                });
            });
        </script>
        <!---//End-rate---->
        <link href="client/css/form.css" rel="stylesheet" type="text/css" media="all" />
    </head>
    <body>

        <?php include 'command/headerclient.php'; ?>
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h1>Sản phẩm</h1>
                <em></em>
                <h2><a href="index.html">Trang chủ</a><label>/</label>Sản phẩm</a></h2>
            </div>
        </div>
        <!--content-->
        <div class="product">
            <div class="container">
                <div class="col-md-9">
                    <div class="mid-popular">

                        <?php
                        $con = new database();
                        $sql = "SELECT * FROM `sanpham` WHERE 1 = 1";
                        if (isset($_GET['loai']) ? $sql.=" AND MaDanhMuc = " ."'". $_GET['loai']."'" : $sql = $sql)
                            ;
                        $sanpham = $con->select_all_query($sql);

                        if (!$sanpham) {
                            echo "Chua co san pham trong danh muc";
                        } else {

                            foreach ($sanpham as $key => $value) {
                                ?>
                                <div class="col-md-4 item-grid1 simpleCart_shelfItem">
                                    <div class=" mid-pop">
                                        <div class="pro-img">
                                            <img src="client/images/<?php echo $value['Hinh']; ?>" class="img-responsive" alt="">
                                            <div class="zoom-icon ">
                                                <a class="picture" href="client/images/<?php echo $value['Hinh']; ?>" rel="title" class="b-link-stripe b-animate-go  thickbox"><i class="glyphicon glyphicon-search icon "></i></a>
                                                <a href="sanphamchitiet.php?masp=<?php echo $value['MaSanPham'];?>"><i class="glyphicon glyphicon-menu-right icon"></i></a>
                                            </div>
                                        </div>
                                        <div class="mid-1">
                                            <div class="women">
                                                <div class="women-top">
                                                  
                                                    <h6><a href="sanphamchitiet.php"><?php echo $value['TenSanPham'];?></a></h6>
                                                </div>

                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="mid-2">
                                                <p ><em class="item_price"><?php echo $value['Gia'];?></em></p>
                                                <div class="clearfix"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            <?php }
                        }
                        ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-3 product-bottom">
                    <!--categories-->
                    <div class=" rsidebar span_1_of_left">
                        <h4 class="cate">Danh Mục</h4>
                        <ul class="menu-drop">

                            <?php
                            $sql = "SELECT * FROM `danhmuc` WHERE 1";

                            $sanpham = $con->select_all_query($sql);

                            if (!$sanpham) {
                                echo "Chưa có sản phẩm trong danh mục này";
                            } else {
                                foreach ($sanpham as $type) {
                                    ?>
                                    <li class="item1"><a href="sanpham.php?loai=<?php echo $type['MaDanhMuc']; ?>"><?php echo $type['TenDanhMuc']; ?></a>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>

                </div>
            </div class="clearfix"></div>
        <!--products-->

        <!--//products-->
        <!--brand-->
        <div class="container">
            <div class="brand">
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic1.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic2.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic3.png" class="img-responsive" alt="">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!--//brand-->
    </div>

</div>
<!--//content-->
<!--//footer-->
<?php include 'command/footerclient.php'; ?>
<!--//footer-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="client/js/simpleCart.min.js"></script>
<!-- slide -->
<script src="clientjs/bootstrap.min.js"></script>
<!--light-box-files -->
<script src="client/js/jquery.chocolat.js"></script>
<link rel="stylesheet" href="client/css/chocolat.css" type="text/css" media="screen" charset="utf-8">
<!--light-box-files -->
<script type="text/javascript" charset="utf-8">
            $(function () {
                $('a.picture').Chocolat();
            });
</script>
</body>
</html>