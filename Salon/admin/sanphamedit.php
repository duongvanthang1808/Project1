
<!DOCTYPE HTML>
<html>
    <head>
        <title>Thêm danh mục</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Minimal Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' />
        <!-- Custom Theme files -->
        <link href="css/style.css" rel='stylesheet' type='text/css' />
        <link href="css/font-awesome.css" rel="stylesheet"> 
        <script src="js/jquery.min.js"></script>
        <!-- Mainly scripts -->
        <script src="js/jquery.metisMenu.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <!-- Custom and plugin javascript -->
        <link href="css/custom.css" rel="stylesheet">
        <script src="js/custom.js"></script>
        <script src="js/screenfull.js"></script>
        <script>
            $(function () {
                $('#supported').text('Supported/allowed: ' + !!screenfull.enabled);

                if (!screenfull.enabled) {
                    return false;
                }



                $('#toggle').click(function () {
                    screenfull.toggle($('#container')[0]);
                });



            });
        </script>

        <!----->

        <!--pie-chart--->
        <script src="js/pie-chart.js" type="text/javascript"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $('#demo-pie-1').pieChart({
                    barColor: '#3bb2d0',
                    trackColor: '#eee',
                    lineCap: 'round',
                    lineWidth: 8,
                    onStep: function (from, to, percent) {
                        $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });

                $('#demo-pie-2').pieChart({
                    barColor: '#fbb03b',
                    trackColor: '#eee',
                    lineCap: 'butt',
                    lineWidth: 8,
                    onStep: function (from, to, percent) {
                        $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });

                $('#demo-pie-3').pieChart({
                    barColor: '#ed6498',
                    trackColor: '#eee',
                    lineCap: 'square',
                    lineWidth: 8,
                    onStep: function (from, to, percent) {
                        $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                    }
                });


            });

        </script>
        <!--skycons-icons-->
        <script src="js/skycons.js"></script>
        <!--//skycons-icons-->
    </head>
    <body>
        <div id="wrapper">

            <!----->
            <?php include 'command/header.php'; ?>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <div class="content-main">

                    <!--banner-->	
                    <div class="banner">

                        <h2>

                            <span>Thêm danh mục</span>
                        </h2>
                    </div>
                    <div class="validation-system">

                        <div class="validation-form">
                            <!---->

                            <form method="post" action="sanphamedited.php?masp=<?php echo $_GET['masp'] ?>" enctype="multipart/form-data">

                                <?php
                                $con = new database();
                                $sql = "select * from sanpham where MaSanPham = " . "'" . $_GET['masp'] . "'";

                                $sp = $con->select_query($sql);
                                ?>


                                <div class="col-md-4 form-group2 group-mail">
                                    <label class="control-label">Chọn</label>
                                    <select name="tendanhmuc">
                                        <?php
                                        $con = new database();
                                        $sql = "SELECT * FROM danhmuc";
                                        $danhmuc = $con->select_all_query($sql);

                                        foreach ($danhmuc as $type) {
                                            ?>
                                            <option value="<?php echo $type['MaDanhMuc'] ?>"><?php echo $type['TenDanhMuc'] ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"> </div>
                                <div class="col-md-4 form-group1 group-mail">
                                    <label class="control-label">Tên sản phẩm</label>
                                    <input type="text" placeholder="Nhập tên sản phẩm" value="<?php echo $sp['TenSanPham'] ?>" name="tensp">
                                </div>
                                <div class="clearfix"> </div>
                                <div class="col-md-4 form-group1 group-mail">
                                    <label class="control-label">Giá</label>
                                    <input type="text" placeholder="Nhập giá" value="<?php echo $sp['Gia'] ?>" name="gia">
                                </div>
                                <div class="clearfix"> </div>
                                <div class="col-md-4 form-group1 group-mail">
                                    <label class="control-label">Tiêu đề</label>
                                    <input type="text" placeholder="Nhập tiêu đề" value="<?php echo $sp['TieuDe'] ?>" name="tieude">
                                </div>
                                <div class="clearfix"> </div>
                                <div class="col-md-4 form-group1 group-mail">
                                    <label class="control-label">Hình</label>
                                    <input type="file" name="hinhanh">
                                </div>
                                <div class="clearfix"> </div>
                                <div class="col-md-4 form-group">
                                    <button type="submit" class="btn btn-primary">Sửa</button>
                                    <button type="reset" class="btn btn-default">Nhập lại</button>
                                </div>
                                <div class="clearfix"> </div>

                            </form>

                            <!--//banner-->


                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <!---->
                <!--scrolling js-->
                <script src="js/jquery.nicescroll.js"></script>
                <script src="js/scripts.js"></script>
                <!--//scrolling js-->
                <script src="js/bootstrap.min.js"></script>
                </body>
                </html>

