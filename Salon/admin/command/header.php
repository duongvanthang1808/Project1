<?php require "../database/database.php"; ?>

<nav class="navbar-default navbar-static-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <h1> <a class="navbar-brand" href="danhmuc.php">Shoppin</a></h1>         
    </div>
    <div class=" border-bottom">



        <!-- Brand and toggle get grouped for better mobile display -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="drop-men" >
            <ul class=" nav_1">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret">Rackham<i class="caret"></i></span><img src="images/wo.jpg"></a>
                    <ul class="dropdown-menu " role="menu">
                        <li><a href="profile.html"><i class="fa fa-user"></i>Sửa cá nhân</a></li>      
                        <li><a href="inbox.html"><i class="fa fa-clipboard"></i>Đăng xuất</a></li>
                    </ul>
                </li>

            </ul>
        </div><!-- /.navbar-collapse -->
        <div class="clearfix">

        </div>

        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">



                    <li>
                        <a href="" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Sản phẩm</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="../admin/danhmuc.php" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Danh mục sản phẩm</a></li>

                            <li><a href="../admin/sanpham.php" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i>Sản phẩm</a></li>



                        </ul>
                    </li>
                    <li>
                        <a href="../admin/thanhvien.php" class=" hvr-bounce-to-right"><i class="fa fa-group nav_icon"></i> <span class="nav-label">Thành viên</span> </a>
                    </li>
                    <li>
                        <a href="../admin/hoadon.php" class=" hvr-bounce-to-right"><i class="fa fa fa-book nav_icon"></i> <span class="nav-label">Hóa đơn yêu cầu</span> </a>
                    </li>



                </ul>
            </div>
        </div>
</nav>