
<!DOCTYPE html>
<html>
    <head>
        <title>Shopin A Ecommerce Category Flat Bootstrap Responsive Website Template | Contact :: w3layouts</title>
        <link href="client/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Custom Theme files -->
        <!--theme-style-->
        <link href="client/css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Shopin Responsive web template, Bootstrap Web Templates, Flat Web Templates, AndroId Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--theme-style-->
        <link href="client/css/style4.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <script src="client/js/jquery.min.js"></script>
        <!--- start-rate---->
        <script src="client/js/jstarbox.js"></script>
        <link rel="stylesheet" href="client/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
        <script type="text/javascript">
            jQuery(function () {
                jQuery('.starbox').each(function () {
                    var starbox = jQuery(this);
                    starbox.starbox({
                        average: starbox.attr('data-start-value'),
                        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                        ghosting: starbox.hasClass('ghosting'),
                        autoUpdateAverage: starbox.hasClass('autoupdate'),
                        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                        stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function (event, value) {
                        if (starbox.hasClass('random')) {
                            var val = Math.random();
                            starbox.next().text(' ' + val);
                            return val;
                        }
                    })
                });
            });
        </script>
        <!---//End-rate---->
    </head>
    <body>
        <!--header-->
        <?php include 'command/headerclient.php'; ?>

        <div class="banner-top">
            <div class="container">
                <h1>Liên hệ</h1>
                <em></em>
                <h2><a href="index.html">Trang chủ</a><label>/</label>Liên hệ</h2>
            </div>
        </div>
        <div class="contact">

            <div class="contact-form">
                <div class="container">

                    <div class="col-md-8 contact-top">
                        <h3>Hóa đơn mua hàng</h3>
                        <form method="post" action="muahanged.php">
                            <div>
                                <span>Mã hóa đơn</span>		
                                <input type="text" name="ma" value="<?php echo "MHD" . time(); ?>">						
                            </div>
                            <div>
                                <span>Ngày mua</span>		
                                <input type="text" name="ngaymua" value="<?php echo date("Y/m/d"); ?>">						
                            </div>
                            <div>
                                <span>Mã khách</span>	
                                <?php $khach = $_SESSION['KHACHHANG']; ?>
                                <input type="text" name="makhach" value="<?php echo $khach['MaKhachHang']; ?>">	
                            </div>
                            <div>
                                <span>Mã sản phẩm</span>		
                                <input type="text" name="masp" value="<?php echo $_GET['masp']; ?>">
                            </div>
                            <div>
                                <span>Số lượng</span>		
                                <input type="text" name="soluong" value="1">	
                            </div>
                            <div>
                                <?php
                                $con = new database();
                                $sql = "SELECT * from sanpham WHERE MaSanPham = " . "'" . $_GET['masp'] . "'";

                                $sp = $con->select_query($sql);
                                ?>
                                <span>Đơn giá</span>		
                                <input type="text" name="gia" value="<?php echo $sp['Gia']; ?>">	
                            </div>

                            <label class="hvr-skew-backward">
                                <input type="submit" value="Xác nhận">
                            </label>
                        </form>						
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
           
        </div>
        <div class="container">
            <div class="brand">
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic1.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic2.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic3.png" class="img-responsive" alt="">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <!--//content-->
        <?php include 'command/footerclient.php'; ?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <script src="client/js/simpleCart.min.js"></script>
        <!-- slide -->
        <script src="client/js/bootstrap.min.js"></script>

    </body>
</html>