
<!DOCTYPE html>
<html>
    <head>
        <title>Shopin A Ecommerce Category Flat Bootstrap Responsive Website Template | 404 :: w3layouts</title>
        <link href="client/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Custom Theme files -->
        <!--theme-style-->
        <link href="client/css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Shopin Responsive web template, Bootstrap Web Templates, Flat Web Templates, AndroId Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--theme-style-->
        <link href="client/css/style4.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <script src="client/js/jquery.min.js"></script>
        <!--- start-rate---->
        <script src="client/js/jstarbox.js"></script>
        <link rel="stylesheet" href="client/scss/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
        <script type="text/javascript">
            jQuery(function () {
                jQuery('.starbox').each(function () {
                    var starbox = jQuery(this);
                    starbox.starbox({
                        average: starbox.attr('data-start-value'),
                        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                        ghosting: starbox.hasClass('ghosting'),
                        autoUpdateAverage: starbox.hasClass('autoupdate'),
                        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                        stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function (event, value) {
                        if (starbox.hasClass('random')) {
                            var val = Math.random();
                            starbox.next().text(' ' + val);
                            return val;
                        }
                    })
                });
            });
        </script>
        <!---//End-rate---->
    </head>
    <body>
        <!--header-->
        <?php include 'command/headerclient.php'; ?>
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h1>Giới thiệu</h1>
                <em></em>
                <h2><a href="index.html">Trang chủ</a><label>/</label>Giới thiệu</a></h2>
            </div>
        </div>
        <!--login-->
        <div class="container">
            <div class="four">

                <div class="col-md-6">
                    <p>Website của chúng tôi đã có nhiều năm kinh nghiệm về tư vấn những sản phẩm hiệu quả nhất cho mái tóc của bạn. Chúng tôi luôn cố gắng tư vấn các sản phẩm tốt nhất đến với các bạn phù hợp với từng kiểu tóc cũng như khả năng phục hồi tóc của các bạn.</p>
                    <p>Để đảm bảo chất lượng tốt nhất, hãy chọn lọc cũng như tìm hiểu kỹ các sản phẩm của chúng tôi, đặc biệt bạn có thể liên hệ để được chúng tôi tư vấn để có hiệu quả tốt nhất.</p>
                    <p>Tiêu chí của chúng tôi là “chuyên nghiệp – hiệu quả” cho từng khách hàng khi đến với chúng tôi. Với đội ngũ tư vấn viên, kỹ thuật viên nhiệt tình năng nổ, hoạt bát chúng tôi đã tạo được niềm tin trong khách hàng rất lớn. Bên cạnh đó việc tiếp thu các ý kiến đóng góp của tất cả các bạn đã làm cho chúng tôi trở nên hoàn thiện hơn trong công việc.</p>

                </div>
                <div class="col-md-6">
                    <p>Sản phẩm chăm sóc tóc với những thương hiệu nổi tiếng trong và ngoài nước được đánh giá cao về chất lượng cũng như giá cả. Chúng tôi hy vọng sẽ mang đến cho các bạn một giải pháp hiệu quả và tiết kiệm nhất cho mái tóc, mang đến sự thỏa mãn, hài lòng nhất cho các bạn.</p>

                    <p>Đến với salontoc của chúng tôi, bạn sẽ luôn cảm thấy thoải mái, hài lòng với tất cả các sản phẩm cũng như phong cách phục vụ chuyên nghiệp, năng động và nhiệt tình của nhân viên.</p>

                    <p>Hãy liên hệ với chúng tôi khi bạn cần . Xin chân thành cảm ơn!</p>
                </div>
            </div>
        </div>
        <!--//login-->

        <!--brand-->
        <div class="container">
            <div class="brand">
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic1.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic2.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic3.png" class="img-responsive" alt="">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!--//brand-->
    </div>

</div>
<!--//content-->
<!--//footer-->
<?php include 'command/footerclient.php'; ?>
<!--//footer-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="client/js/simpleCart.min.js"></script>
<!-- slide -->
<script src="client/js/bootstrap.min.js"></script>

</body>
</html>