-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2016 at 11:59 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `salontoc`
--

-- --------------------------------------------------------

--
-- Table structure for table `cthoadon`
--

CREATE TABLE `cthoadon` (
  `MaHoaDon` varchar(20) NOT NULL,
  `MaSanPham` varchar(20) NOT NULL,
  `SoLuong` int(11) NOT NULL,
  `DonGia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cthoadon`
--

INSERT INTO `cthoadon` (`MaHoaDon`, `MaSanPham`, `SoLuong`, `DonGia`) VALUES
('MHD1465652437', 'SP01', 1, 270000),
('MHD1465654219', 'SP01', 3, 270000),
('MHD1465665677', 'SP01', 1, 270000),
('MHD1465666114', 'SP01', 1, 270000);

-- --------------------------------------------------------

--
-- Table structure for table `danhmuc`
--

CREATE TABLE `danhmuc` (
  `MaDanhMuc` varchar(20) NOT NULL,
  `TenDanhMuc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `danhmuc`
--

INSERT INTO `danhmuc` (`MaDanhMuc`, `TenDanhMuc`) VALUES
('DM01', 'Sáp vuốt tóc'),
('DM02', 'Gôm giữ nếp'),
('DM03', 'Gel giữ nếp'),
('DM1465666150', 'Tesst');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `ma` varchar(20) NOT NULL,
  `ten` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `chude` text NOT NULL,
  `noidung` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`ma`, `ten`, `email`, `chude`, `noidung`) VALUES
('FB1465640538', 0, 'dvthang1808@gmail.com', 'Phản hồi nội dung', 'Trang web rất đẹp '),
('FB1465665852', 0, 'dvthang1808@gmail.com', 'FAFDSA', ' ÀDAFD');

-- --------------------------------------------------------

--
-- Table structure for table `hoadon`
--

CREATE TABLE `hoadon` (
  `MaHoaDon` varchar(20) NOT NULL,
  `NgayHD` date NOT NULL,
  `MaKH` varchar(20) NOT NULL,
  `TriGia` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hoadon`
--

INSERT INTO `hoadon` (`MaHoaDon`, `NgayHD`, `MaKH`, `TriGia`) VALUES
('MHD1465652437', '0000-00-00', 'KH1465638717', 270000),
('MHD1465654219', '0000-00-00', 'KH1465638717', 810000),
('MHD1465665677', '2016-06-11', 'KH1465638717', 270000),
('MHD1465666114', '2016-06-11', 'KH1465638717', 270000);

-- --------------------------------------------------------

--
-- Table structure for table `khachhang`
--

CREATE TABLE `khachhang` (
  `MaKhachHang` varchar(20) NOT NULL,
  `TenKhachHang` varchar(100) NOT NULL,
  `GioiTinh` tinyint(4) NOT NULL,
  `DiaChi` varchar(50) NOT NULL,
  `DienThoai` varchar(20) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khachhang`
--

INSERT INTO `khachhang` (`MaKhachHang`, `TenKhachHang`, `GioiTinh`, `DiaChi`, `DienThoai`, `Email`, `Password`) VALUES
('KH1465638717', 'Dương Văn Thắng', 1, 'Hà Nội', '01642474811', 'dvthang1808@gmail.com', '123123');

-- --------------------------------------------------------

--
-- Table structure for table `sanpham`
--

CREATE TABLE `sanpham` (
  `MaSanPham` varchar(20) NOT NULL,
  `MaDanhMuc` varchar(20) NOT NULL,
  `TenSanPham` varchar(50) NOT NULL,
  `Gia` int(11) NOT NULL,
  `TieuDe` text NOT NULL,
  `Hinh` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sanpham`
--

INSERT INTO `sanpham` (`MaSanPham`, `MaDanhMuc`, `TenSanPham`, `Gia`, `TieuDe`, `Hinh`) VALUES
('SP01', 'DM01', 'Sáp Hair Mud Colonna', 270000, 'Colonna Texture là dòng sáp được sản xuất bởi công nghệ và công thức của Đức với khả năng giữ nếp tốt, cùng độ bóng tự nhiên mà không làm khô tóc, giúp phái nam dễ dàng tạo cho mình những kiểu tóc chuẩn men. Colonna phù hợp với nhiều kiểu tóc như Quiff, Side Part, Side swept, ...Đây là dòng sáp được chính stylist tại 30Shine sử dụng bởi sự linh hoạt vượt trội so với các sản phẩm khác cùng tầm giá.', 'image-1465723715.jpg'),
('SP1465724497', 'DM02', 'Gôm Colonna', 200000, 'Giữ nếp lâu, không làm bóng tóc, mùi hương dễ chịu, có thể dùng kết hợp cùng nhiều loại sá.', 'image-1465724497.jpg'),
('SP1465725481', 'DM02', 'Gôm Butterfly 320ml', 130000, ' Siêu cứng, độ bóng tự nhiên, mùi hương hoa quả quyến rũ đặc biệt, tạo cảm giác thoải mái khi sử dụng', 'image-1465725481.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cthoadon`
--
ALTER TABLE `cthoadon`
  ADD PRIMARY KEY (`MaHoaDon`,`MaSanPham`),
  ADD KEY `MaSanPham` (`MaSanPham`);

--
-- Indexes for table `danhmuc`
--
ALTER TABLE `danhmuc`
  ADD PRIMARY KEY (`MaDanhMuc`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`ma`);

--
-- Indexes for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD PRIMARY KEY (`MaHoaDon`,`MaKH`),
  ADD KEY `MaKH` (`MaKH`);

--
-- Indexes for table `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`MaKhachHang`);

--
-- Indexes for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`MaSanPham`),
  ADD KEY `MaDanhMuc` (`MaDanhMuc`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cthoadon`
--
ALTER TABLE `cthoadon`
  ADD CONSTRAINT `cthoadon_ibfk_1` FOREIGN KEY (`MaHoaDon`) REFERENCES `hoadon` (`MaHoaDon`),
  ADD CONSTRAINT `cthoadon_ibfk_2` FOREIGN KEY (`MaSanPham`) REFERENCES `sanpham` (`MaSanPham`);

--
-- Constraints for table `hoadon`
--
ALTER TABLE `hoadon`
  ADD CONSTRAINT `hoadon_ibfk_1` FOREIGN KEY (`MaKH`) REFERENCES `khachhang` (`MaKhachHang`);

--
-- Constraints for table `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `sanpham_ibfk_1` FOREIGN KEY (`MaDanhMuc`) REFERENCES `danhmuc` (`MaDanhMuc`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
