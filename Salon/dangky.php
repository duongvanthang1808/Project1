
<!DOCTYPE html>
<html>
    <head>
        <title>Shopin A Ecommerce Category Flat Bootstrap Responsive Website Template | Register :: w3layouts</title>
        <link href="client/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Custom Theme files -->
        <!--theme-style-->
        <link href="client/css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Shopin Responsive web template, Bootstrap Web Templates, Flat Web Templates, AndroId Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--theme-style-->
        <link href="client/css/style4.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <script src="client/js/jquery.min.js"></script>
        <!--- start-rate---->
        <script src="client/js/jstarbox.js"></script>
        <link rel="stylesheet" href="client/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
        <script type="text/javascript">
            jQuery(function () {
                jQuery('.starbox').each(function () {
                    var starbox = jQuery(this);
                    starbox.starbox({
                        average: starbox.attr('data-start-value'),
                        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                        ghosting: starbox.hasClass('ghosting'),
                        autoUpdateAverage: starbox.hasClass('autoupdate'),
                        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                        stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function (event, value) {
                        if (starbox.hasClass('random')) {
                            var val = Math.random();
                            starbox.next().text(' ' + val);
                            return val;
                        }
                    })
                });
            });
        </script>
        <!---//End-rate---->
    </head>
    <body>
        <!--header-->
        <?php include 'command/headerclient.php'; ?>
        <!--banner-->
        <div class="banner-top">
            <div class="container">
                <h1>Register</h1>
                <em></em>
                <h2><a href="index.html">Home</a><label>/</label>Register</a></h2>
            </div>
        </div>
        <!--login-->
        <div class="container">
            <div class="login">
                <form method="post" action="dangkyed.php">
                    <div class="col-md-6 login-do">
                        <div class="login-mail">
                            <input type="text" placeholder="Tên khách hàng" name="ten">
                            <i  class="glyphicon glyphicon-user"></i>
                        </div>
                        <div class="login-mail">
                            <select name="phai">
                                <option selected="selected" value="1">Nam</option>
                                <option value="0">Nữ</option>
                            </select>
                            <i  class="glyphicon glyphicon-user"></i>
                        </div>
                        <div class="login-mail">
                            <input type="text" placeholder="Địa chỉ" name="diachi">
                            <i  class="glyphicon glyphicon-home"></i>
                        </div>
                        <div class="login-mail">
                            <input type="text" placeholder="Điện thoại" name="dienthoai">
                            <i  class="glyphicon glyphicon-phone"></i>
                        </div>
                        <div class="login-mail">
                            <input type="text" placeholder="Email" name="email">
                            <i class="glyphicon glyphicon-envelope"></i>
                        </div>
                        <div class="login-mail">
                            <input type="password" placeholder="Mật khẩu" name="matkhau">
                            <i class="glyphicon glyphicon-lock"></i>
                        </div>

                        <label class="hvr-skew-backward">
                            <input type="submit" value="Submit">
                        </label>

                    </div>
                    <div class="col-md-6 login-right">
                        <h3>Đăng ký tài khoản miễn phí</h3>

                        <p>Đã có tài khoản thì hãy đăng nhập</p>
                        <a href="dangnhap.php" class="hvr-skew-backward">Đăng nhập</a>

                    </div>

                    <div class="clearfix"> </div>
                </form>
            </div>

        </div>

        <!--//login-->

        <!--brand-->
        <div class="container">
            <div class="brand">
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic1.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic2.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic3.png" class="img-responsive" alt="">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <!--//brand-->
    </div>

</div>
<!--//content-->
<!--//footer-->
<?php include 'command/footerclient.php'; ?>
<!--//footer-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script src="client/js/simpleCart.min.js"></script>
<!-- slide -->
<script src="client/js/bootstrap.min.js"></script>

</body>
</html>