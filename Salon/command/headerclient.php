<?php
session_start();
require 'database/database.php';
?>
<div class="header">
    <div class="container">
        <div class="head">
            <div class="logo">
                <a href="index.php"><img style="z-index: 10;" src="client/images/logo.png" alt=""></a>	
            </div>
        </div>
    </div>
    <div class="header-top">
        <div class="container">
            <div class="col-sm-5 col-md-offset-2  header-login">
                <ul>
                    <?php
                    if (isset($_SESSION['KHACHHANG'])) {
                        $khach = $_SESSION['KHACHHANG'];
                        ?>
                        <li ><a><?php echo $khach['TenKhachHang']; ?></a>
                            <ul>
                                <li><a href="dangxuat.php">Đăng xuất</a>
                            </ul>
                        </li>
                    <?php } else { ?>
                        <li><a href="dangnhap.php">Đăng nhập</a>
                        <?php } ?>
                    <li><a href="dangky.php">Đăng ký</a></li>

                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <div class="container">

        <div class="head-top">

            <div class="col-sm-8 col-md-offset-2 h_menu4">
                <nav class="navbar nav_bottom" role="navigation">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header nav_2">
                        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div> 
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                        <ul class="nav navbar-nav nav_1">
                            <li><a class="color" href="index.php">Trang chủ</a></li>

                            <li><a class="color4" href="sanpham.php">Sản phẩm </a></li>
                            <li><a class="color4" href="gioithieu.php">Giới thiệu </a></li>
                            <li><a class="color6" href="lienhe.php">Liên hệ</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->

                </nav>
            </div>

            <div class="col-sm-2 search-right">

                <div class="cart box_1">
					<a href="giohang.php">
						<h3> <div class="total">
						<span>100$</span></div>
						<img src="client/images/cart.png" alt=""/></h3>
					</a>
						<p><a href="javascript:;" class="simpleCart_empty">Empty Cart</a></p>

					</div>
					<div class="clearfix"> </div>
               

                <!----->

                <!---pop-up-box---->					  
                <link href="client/css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
                <script src="client/js/jquery.magnific-popup.js" type="text/javascript"></script>
                <!---//pop-up-box---->
                
                <script>
                    $(document).ready(function () {
                        $('.popup-with-zoom-anim').magnificPopup({
                            type: 'inline',
                            fixedContentPos: false,
                            fixedBgPos: true,
                            overflowY: 'auto',
                            closeBtnInside: true,
                            preloader: false,
                            midClick: true,
                            removalDelay: 300,
                            mainClass: 'my-mfp-zoom-in'
                        });

                    });
                </script>		
                <!----->
                <div class="clearfix"></div>
            </div>	
        </div>	
    </div>