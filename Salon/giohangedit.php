<?php
require 'database/database.php';
require 'command/cart.php';

session_start ();

if (isset ( $_SESSION ['CART'] )) {
	$cart = $_SESSION ['CART'];
}

$masp = $_POST ['id'];
$quantity = $_POST ['quantity'];

foreach ( $cart as $value ) {
	if ($value->sanpham ['MaSanPham'] == $masp) {
		$value->quantity = $quantity;
	}
}

$_SESSION ['CART'] = $cart;

die(true);