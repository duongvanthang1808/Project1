
<!DOCTYPE html>
<html>
    <head>
        <title>Shopin A Ecommerce Category Flat Bootstrap Responsive Website Template | Contact :: w3layouts</title>
        <link href="client/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Custom Theme files -->
        <!--theme-style-->
        <link href="client/css/style.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Shopin Responsive web template, Bootstrap Web Templates, Flat Web Templates, AndroId Compatible web template, 
              Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--theme-style-->
        <link href="client/css/style4.css" rel="stylesheet" type="text/css" media="all" />	
        <!--//theme-style-->
        <script src="client/js/jquery.min.js"></script>
        <!--- start-rate---->
        <script src="client/js/jstarbox.js"></script>
        <link rel="stylesheet" href="client/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
        <script type="text/javascript">
            jQuery(function () {
                jQuery('.starbox').each(function () {
                    var starbox = jQuery(this);
                    starbox.starbox({
                        average: starbox.attr('data-start-value'),
                        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                        ghosting: starbox.hasClass('ghosting'),
                        autoUpdateAverage: starbox.hasClass('autoupdate'),
                        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                        stars: starbox.attr('data-star-count') || 5
                    }).bind('starbox-value-changed', function (event, value) {
                        if (starbox.hasClass('random')) {
                            var val = Math.random();
                            starbox.next().text(' ' + val);
                            return val;
                        }
                    })
                });
            });
        </script>
        <!---//End-rate---->
    </head>
    <body>
        <!--header-->
        <?php include 'command/headerclient.php'; ?>

        <div class="banner-top">
            <div class="container">
                <h1>Liên hệ</h1>
                <em></em>
                <h2><a href="index.html">Trang chủ</a><label>/</label>Liên hệ</h2>
            </div>
        </div>
        <div class="contact">

            <div class="contact-form">
                <div class="container">
                    <div class="col-md-6 contact-left">
                        <h3>Hãy liên hệ với chúng tôi khi bạn cần. </h3>
                        <p>Để đảm bảo chất lượng tốt nhất, hãy chọn lọc cũng như tìm hiểu kỹ các sản phẩm của chúng tôi, đặc biệt bạn có thể liên hệ để được chúng tôi tư vấn để có hiệu quả tốt nhất.</p>


                        <div class="address">
                            <div class=" address-grid">
                                <i class="glyphicon glyphicon-map-marker"></i>
                                <div class="address1">
                                    <h3>Địa chỉ</h3>
                                    <p>282 Xã Đàn, Đống Đa, Hà Nội</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class=" address-grid ">
                                <i class="glyphicon glyphicon-phone"></i>
                                <div class="address1">
                                    <h3>Phone:</h3><h3>
                                        <p>043678562</p>
                                    </h3></div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class=" address-grid ">
                                <i class="glyphicon glyphicon-envelope"></i>
                                <div class="address1">
                                    <h3>Email:</h3>
                                    <p><a href="mailto:info@example.com">lolem@gmail.com</a></p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                            <div class=" address-grid ">
                                <i class="glyphicon glyphicon-bell"></i>
                                <div class="address1">
                                    <h3>Giờ mở cửa: </h3>
                                    <p>Thứ Hai - Chủ Nhật, 7AM - 10PM</p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 contact-top">
                        <h3>Gửi phản hồi cho chúng tôi</h3>
                        <form method="post" action="feedback.php">
                            <div>
                                <span>Tên bạn </span>		
                                <input type="text" name="ten">						
                            </div>
                            <div>
                                <span>Email của bạn </span>		
                                <input type="text" name="email">						
                            </div>
                            <div>
                                <span>Chủ đề</span>		
                                <input type="text" name="chude">	
                            </div>
                            <div>
                                <span>Nội dung</span>		
                                <textarea name="noidung"> </textarea>	
                            </div>
                            <label class="hvr-skew-backward">
                                <input type="submit" value="Gửi">
                            </label>
                        </form>						
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="map">
                <iframe src="https://www.google.com/maps/d/u/0/embed?mid=12Pmk9LQhFVJ6O2T41IvHGwEivY8" width="640" height="480"></iframe>
            </div>
        </div>
        <div class="container">
            <div class="brand">
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic1.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic2.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-3 brand-grid">
                    <img src="client/images/ic3.png" class="img-responsive" alt="">
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <!--//content-->
        <?php include 'command/footerclient.php'; ?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

        <script src="client/js/simpleCart.min.js"></script>
        <!-- slide -->
        <script src="client/js/bootstrap.min.js"></script>

    </body>
</html>